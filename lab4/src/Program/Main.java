package Program;

import Parking.*;

import java.util.Scanner;

public class Main {
    private static Scanner in = new Scanner(System.in);
    private static Parking parking;
    public static void main(String[] args) {
        int n = 0;
        while(n <= 0 || n > 100){
            System.out.println("Введите кол-во парковочных мест на парковке:");
            n = in.nextInt();
        }
        parking = new Parking(n);
        menu();
    }

    private static void menu() {
        System.out.println("Выберите действие:");
        System.out.println("1. Припарковать машину.");
        System.out.println("2. Уехать с парковки.");
        System.out.println("3. Состояние парковки.");
        System.out.print("Номер пункта меню: ");
        int num = in.nextInt();
        selectedAction(num);
    }

    private static void selectedAction(int num) {
        String str;
        switch (num) {
            case 1:
                int index = parking.parkTheCar();
                if(index != 0){
                    System.out.println("Ваше место на парковке: " + index);
                }else{
                    System.out.println("На парковке больше нет места.");
                }
                str = in.nextLine();
                menu();
                break;
            case 2:
                System.out.println("Укажите Ваш номер места на парковке: ");
                int n = in.nextInt();
                if(parking.leaveParking(n)){
                    System.out.println("Вы успешно покинули парковку.");
                }else{
                    System.out.println("Данное место не найдено либо свободно.");
                }
                str = in.nextLine();
                menu();
                break;
            case 3:
                System.out.println(parking.toString());
                str = in.nextLine();
                menu();
                break;
            default:
                System.out.print("Неверное число.");
                str = in.nextLine();
                menu();
                break;
        }
    }
}
