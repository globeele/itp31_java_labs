package Parking;

import java.util.ArrayList;
import java.util.List;

public class Parking {
    private List<Place> places;
    private int countOfPlaces;

    public  Parking(int n){
        countOfPlaces = n;
        places = new ArrayList<Place>();
        generatePlaces();
    }

    private void generatePlaces(){
        for(int i = 0; i < countOfPlaces; i++){
            places.add(new Place());
        }
    }

    public int parkTheCar(){
        for(int i = 0; i < countOfPlaces; i++){
            if(!places.get(i).isBusy()){
                places.get(i).rank();
                return (i+1);
            }
        }
        return 0;
    }

    public boolean leaveParking(int index){
        if(index <= countOfPlaces){
            if(places.get(index - 1).isBusy()){
                places.get(index - 1).leave();
                return true;
            }else
                return false;
        }else
            return false;
    }

    @Override
    public String toString(){
        String text = "";
        String busy;
        for(int i = 0; i < countOfPlaces; i++){
            busy = places.get(i).isBusy() ? "Занято" : "Свободно";
            text += (i+1) + ". " + busy + "\n";
        }
        return text;
    }
}
