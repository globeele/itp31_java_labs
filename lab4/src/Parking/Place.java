package Parking;

public class Place {
    private boolean busy;
    public Place(){
        busy = false;
    }

    public boolean isBusy(){
        return busy;
    }

    public void rank(){
        busy = true;
    }

    public void leave(){
        busy = false;
    }
}
