package com.db;

public class TourType {
    private int tourID;
    private String tour;
    private boolean hotTour;

    public TourType(int tourID, String tour, boolean hotTour) {
        this.tourID = tourID;
        this.tour = tour;
        this.hotTour = hotTour;
    }

    public int getTourID() {
        return tourID;
    }

    public String getTour() {
        return tour;
    }

    public boolean isHotTour() {
        return hotTour;
    }

    @Override
    public String toString() {
        return "\n TourType{" +
                "tourID=" + tourID +
                ", tour='" + tour + '\'' +
                ", hotTour=" + hotTour +
                '}';
    }
}
