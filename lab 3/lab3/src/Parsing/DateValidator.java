package Parsing;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateValidator {
    private static final String DATE_PATTERN
            = "(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])"
            + "/((1[6-9]\\d\\d)|([2-9])\\d\\d\\d)";
    private Pattern pattern;
    private Matcher matcher;
    public DateValidator() {
        pattern = Pattern.compile(DATE_PATTERN);
    }

    public Boolean validate(String date){
        matcher = pattern.matcher(date);
        if(matcher.matches()){
            matcher.reset();
            if(matcher.find()){
                String day = matcher.group(1);
                String month = matcher.group(2);
                int year = Integer.parseInt(matcher.group(3));

                if("31".equals(day)){
                    return Arrays.asList(new String[]{"01", "03", "05", "07", "08", "10", "12"})
                            .contains(month);
                }else if("02".equals(month)){
                    Integer parsedDate = Integer.parseInt(day);
                    if(year % 4 == 0){
                        if(year % 100 == 0){
                            if(year % 400 == 0){
                                //Високосный год
                                return parsedDate <= 29;
                            }
                            //Невисокосный год
                            return parsedDate <= 28;
                        }
                        //Високосный год
                        return parsedDate <= 29;
                    }else{
                        //Невисокосный год
                        return parsedDate <= 28;
                    }
                }else{
                    return true;
                }

            }
        }
        return false;
    }
}
