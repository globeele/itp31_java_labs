package Parsing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Text {

    public List<Word> words = new ArrayList<Word>();

    public Text(String text){
        getWords(text);
    }

    private void getWords(String text){
        Pattern wordPattern = Pattern.compile("([а-яА-Я]+)");
        Matcher wordMatcher = wordPattern.matcher(text);
        while(wordMatcher.find()){
            words.add(new Word(wordMatcher.group()));
        }
    }

    public String sortWords(){
        String sortText = "";
        Collections.sort(words);
        for(int i = 0; i < words.size() - 1; i++){
            sortText += words.get(i).getWord() + " ";
            if(Character.toLowerCase(words.get(i).getWord().charAt(0)) != Character.toLowerCase(words.get(i+1).getWord().charAt(0))) {
                StringBuilder stringBuilder = new StringBuilder(words.get(i+1).getWord());
                stringBuilder.setCharAt(0, Character.toUpperCase(words.get(i+1).getWord().charAt(0)));
                words.get(i+1).setWord(stringBuilder.toString());
                //words.get(i+1).getWord().toCharArray()[0] = Character.toUpperCase(words.get(i+1).getWord().charAt(0));
//                System.out.println(words.get(i+1).getWord());
                sortText += "\n" + words.get(i+1).getWord() + " ";
                i=i+1;
            }
        }
        sortText += words.get(words.size()-1).getWord() + " ";
        return sortText;
    }
}
