package Parsing;

import java.util.Objects;

public class Word implements Comparable<Word>{
    private String word;

    private int length;
    public String getWord() {
        return word;
    }

    public int getLength() {
        return length;
    }

    public Word(String word)
    {
        this.word = word;
        this.length = word.length();
    }

    @Override
    public String toString() {
        return this.getWord();
    }

    @Override
    public int compareTo(Word word) {
        char let1, let2;

        let1= Character.toLowerCase(this.getWord().charAt(0));
        let2 = Character.toLowerCase(word.getWord().charAt(0));

        if (let1 == let2) {
            return 0;
        } else if (let1 < let2) {
            return -1;
        } else {
            return 1;
        }
    }


    public void setWord(String word) {
        this.word = word;
    }
}

