package Program;

import Parsing.*;
import java.io.*;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class);
    public static void main(String[] args) {
        FileInputStream fis;
        String stringText, stringDates;
        Properties property = new Properties();
        try {

            fis = new FileInputStream("src/resources/config.properties");
            property.load(fis);
            String DATEPATH = property.getProperty("DATEPATH");
            String TEXTPATH = property.getProperty("TEXTPATH");
            fis.close();

            DateValidator dateValidator = new DateValidator();

            stringDates = readFile(DATEPATH);
            String[] dates = stringDates.split("\n");
            for(String date : dates){
                if(dateValidator.validate(date)) {
                    logger.info(date + " - правильно");
                }
                else
                    logger.info(date + " - неправильно");
            }
            stringText = readFile(TEXTPATH);
            Parsing.Text text = new Parsing.Text(stringText);
            logger.info(text.sortWords());
        } catch (FileNotFoundException e) {
            logger.error(e.getLocalizedMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
    private static String readFile(String PATH) throws IOException {
        FileReader fileReader;
        fileReader = new FileReader(PATH);
        BufferedReader reader = new BufferedReader(fileReader);
        StringBuilder sb = new StringBuilder();
        String line = reader.readLine();

        while (line != null) {
            sb.append(line + "\n");
            line = reader.readLine();
        }
        String stringText = sb.toString();
        return stringText;
    }
}
