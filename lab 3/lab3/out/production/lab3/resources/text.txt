Первое, что хочется сказать об этом мультфильме, это то, что он обладает просто колоссальной энергетикой.
Подавляющее большинство современных мультфильмов получаются какими-то пластмассовыми и не несут и десятой доли того заряда энергии, которым обладает Король Лев.
Невозможно не отметить и его потрясающую целостность.
Вместе с прекрасной работой аниматоров, чудеснейшей музыкой и действительно классной озвучкой персонажей создается эта неповторимая атмосфера.
Убежден, что смотреть Король Лев стоит исключительно с оригинальной (английской) звуковой дорожкой либо с любительским (одноголосым) переводом, ибо дубляж лишает львиной доли неповторимой атмосферы мультфильма.
Казалось бы, Король Лев рассказывает обычную, и в какой-то степени предсказуемую историю, так что же в этом может быть особенного?
В этом как раз и заключается его главная ценность: говоря о простых вещах, авторы делают это настолько изящно и естественно, что все персонажи на экране буквально оживают, и ты сопереживаешь им, проживаешь вместе с ними весь мультфильм до самого конца.
При этом Король Лев вызывает настоящую бурю разнообразных эмоций, очень сильных и неповторимых.