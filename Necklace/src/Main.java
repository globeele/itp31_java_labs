import testPackage.*;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) throws IOException {

        Scanner in = new Scanner(System.in);

        int enter;
        List<ForNecklace> forNecklaces = new ArrayList();
        Necklace necklace = new Necklace();
        NecklaceChange necklaceChange = new NecklaceChange();
        forNecklaces.add(necklace);
        forNecklaces.add(necklaceChange);
        while (true){
            int index = Menu();

            switch (index) {
                case 1:

                    System.out.println("Введите количество камней в ожерелье №1:");
                    enter = in.nextInt();
                   // System.out.println("Введите тип камней для ожерелья: ");
                    necklace.createNecklace(enter);
                    necklace.outPut();

                    System.out.println("Введите количество камней в ожерелье №2:");
                    enter = in.nextInt();
                  //  System.out.println("Введите тип камней для ожерелья: ");
                    necklaceChange.createNecklace(enter);
                    necklaceChange.outPut();
                    break;

                case 2 :
                    System.out.println("Карат в ожерелье №1: " + necklace.caratCounting());
                    System.out.println("Карат в ожерелье №2: " + necklaceChange.caratCounting());
                    break;

                case 3:
                    forNecklaces.forEach(ForNecklace::sortingStone);
                    forNecklaces.forEach(ForNecklace::outPut);
                    /*necklace.sortingStone();
                    System.out.println("Сортировка первого ожерелья по названию");
                    necklace.outPut();

                    necklaceChange.sortingStone();
                    System.out.println("Сортировка второго ожерелья по каратам");
                    necklaceChange.outPut();*/
                    break;

                case 4:
                    System.out.println("Введите диапозон карат:");
                    ArrayList<ForStone> stonesFromnecklace = necklace.searchStone(in.nextInt(), in.nextInt());
                    outPutDiap(stonesFromnecklace);

                    System.out.println("Введите диапозон карат:");
                    ArrayList<ForStone> stonesFromnecklaceChange = necklaceChange.searchStone(in.nextInt(), in.nextInt());
                    outPutDiap(stonesFromnecklaceChange);
                    break;

                case 5:
                    return;
            }
        }
    }

        static void outPutDiap(ArrayList<ForStone> stones){
            if (stones.size() == 0){
                System.out.println("В данном диапазоне нет камней!!!");
            }
            for (int i = 0; i < stones.size(); i++) {
                System.out.println(stones.get(i).getName());
                System.out.println(stones.get(i).getCarat());
                System.out.println(stones.get(i).getType());
            }
        }



        //Метод для описания меню
        static int Menu() throws IOException {
            Scanner in = new Scanner(System.in);

            System.out.println("1. Создать ожерелье");
            System.out.println("2. Посчитать ценность в каратах");
            System.out.println("3. Сортировка камней");
            System.out.println("4. Найти камни в диапозоне");
            System.out.println("5. Выход");

            System.out.println("Введите номер меню");
            int index = in.nextInt();

            return index;
        }
}