package testPackage;

//Класс полудрагоценного камня
public class PStone implements ForStone {
    private String name; //название камня
    private int carat; //караты
    private String type; //тип камня

    PStone(String name, int carat, String type) {
        this.name = name;
        this.carat = carat;
        this.type = type;
    }


    public PStone(String s, int parseInt) {
    }

    public String getName() {
        return name;
    }
    public int getCarat() {
        return carat;
    }
    public String getType() {return type; }

    @Override
    public String toString() {
        return this.type.equals("ps") ? "Драгоценные{name='" + this.name + '\'' + ", carat=" + this.carat + '\'' + ", type='" + this.type + '\'' + '}' :
                "Полудрагоценные{name='" + this.name + '\'' + ", carat=" + this.carat + '\'' + "type='" + this.type + '\'' + '}';
    }
}
