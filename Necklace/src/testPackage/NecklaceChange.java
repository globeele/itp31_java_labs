package testPackage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class NecklaceChange implements ForNecklace {
    private Scanner in = new Scanner(System.in);
    private ArrayList<Stone> stones = new ArrayList<>();


    public NecklaceChange() { }

    //Метод для создания ожерелья
    public void createNecklace(int amountStones) throws IOException {
        //чтение файла
        String fileName = "file.txt";
        String[] content =  Files.lines(Paths.get(fileName)).reduce("", String::concat).split(" ");

        stones.clear();

        System.out.println("Доступные камни: ");
        int count = 1;

        //вывод с файла данных о камнях
        for(int i = 0; i < content.length - 2; i = i + 3) {
            System.out.println((count) + ". " + content[i] + "  карат: " + content[i + 1] + " тип: " + content[i+2]);
            count++;
        }

        System.out.println("Выберите камни для ожерелья: ");
        int[] numberStone = new int[amountStones];

        //выбор камней для ожерелья(записываются индексы камнец в массив)
        for (int i = 0; i < amountStones; i++) {
            numberStone[i] = in.nextInt() - 1;
        }

        //запись массива камней для ожерелья
        count = 0;
        for (int i = 0; i < amountStones; i++) {
            stones.add(new Stone(content[numberStone[count] * 3], Integer.parseInt(content[numberStone[count] * 3 + 1]), content[numberStone[count] * 3 + 2] ));
            count = count + 1;
        }

        for (Stone item : stones) {
            System.out.println(item);
        }
    }

    public ArrayList<Stone> getStone() {
        return stones;
    }

    //Метод для вывода информации камней
    public void  outPut() {
        System.out.println(stones.toString());
     //   for (int i = 0; i < stones.size(); i++){
       //     System.out.println(stones.get(i).getName());
         //   System.out.println(stones.get(i).getCarat());
           // System.out.println(stones.get(i).getType() + '\n');
       // }
    }

    //Метод для подсчёта карат
    @Override
    public int caratCounting() {
        int carat = stones.stream().mapToInt(v -> v.getCarat()).sum();
        return carat;
    }

    //Метод для сартировки камней по каратам
    @Override
    public void sortingStone() {

        stones.sort((o1, o2) -> (int) (o1.getCarat() - o2.getCarat()));

        /*Collections.sort(stones, new Comparator<Stone>() {
            @Override
            public int compare(Stone o1, Stone o2) {
                return o1.getCarat() - o2.getCarat();
            }
        });*/

    }

    //Метод для поиска камней в диапазоне (по каратам)
    @Override
    public ArrayList<ForStone> searchStone(int beginning, int end) {
        ArrayList<ForStone> stonesToSearch = new ArrayList<>();
        for (int i = 0; i < stones.size(); i++) {
            if (stones.get(i).getCarat() >= beginning && stones.get(i).getCarat() <= end) {
                stonesToSearch.add(stones.get(i));
            }
        }
        return stonesToSearch;
    }


}
