package testPackage;
import java.util.ArrayList;

//интерфейс для камней
public interface ForStone {
    public String toString();
    public int getCarat();
    public String getName();
    public String getType();
}
