package testPackage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

//Класс ожерелья
public class Necklace implements ForNecklace {

    private Scanner in = new Scanner(System.in);
    private ArrayList<ForStone> stones = new ArrayList<>();


    public Necklace() { }

    //Метод для создания ожерелья
    public void createNecklace(int amountStones) throws IOException {
        //чтение файла
        String fileName = "file.txt";
        String[] content =  Files.lines(Paths.get(fileName)).reduce("", String::concat).split(" ");
        String type = content[2];
        stones.clear();


        System.out.println("Выберите тип камней для ожерелья: ");
        String typeStone = new String(type);


        System.out.println("Доступные камни: ");
        int count = 1;

        //вывод с файла данных о камнях
        for(int i = 0; i < content.length - 2; i = i + 3) {
            System.out.println((count) + ". " + content[i] + "  карат: " + content[i + 1] + " тип: " + content[i+2]);
            count++;
        }


        System.out.println("Выберите камни для ожерелья: ");
        int[] numberStone = new int[amountStones];

        //выбор камней для ожерелья(записываются индексы камней в массив)
        for (int i = 0; i < amountStones; i++) {
            numberStone[i] = in.nextInt() - 1;
        }

        //запись массива камней для ожерелья
        count = 0;
        for (int i = 0; i < amountStones; i++) {
            if (type.equals("s"))   stones.add(new Stone(content[numberStone[count] * 3], Integer.parseInt(content[numberStone[count] * 3 + 1]), content[numberStone[count] * 3 + 2] ));
            else stones.add(new PStone(content[numberStone[count] * 3], Integer.parseInt(content[numberStone[count] * 3 + 1]), content[numberStone[count] * 3 +2]));
            count = count + 1;
        }

        for (ForStone item : stones) {
            System.out.println(item);
        }
    }

    public ArrayList<ForStone> getStones() {
        return stones;
    }


    //Метод для вывода информации камней
    public void  outPut() {
        System.out.println(stones);
//        for (int i = 0; i < stones.size(); i++){
//            System.out.println(stones.get(i).getName());
//            System.out.println(stones.get(i).getCarat());
//        }
    }

    //Метод для подсчёта карат
    @Override
    public int caratCounting() {
        int carat = stones.stream().mapToInt(v -> v.getCarat()).sum();
        return carat;
    }

    //Метод для сортировки камней по названию
    @Override
    public void sortingStone() {
        Collections.sort(stones, new Comparator<ForStone>() {
            @Override
            public int compare(ForStone o1, ForStone o2) {
                    return o1.getName().compareTo(o2.getName());
                }
        });


    }

    //Метод для поиска кмней в диапазоне(по каратам)
    @Override
    public ArrayList<ForStone> searchStone(int beginning, int end) {
        ArrayList<ForStone> stonesToSearch = new ArrayList<>();
        for (int i = 0; i < stones.size(); i++) {
            if (stones.get(i).getCarat() >= beginning && stones.get(i).getCarat() <= end) {
                stonesToSearch.add(stones.get(i));
            }
        }
        return stonesToSearch;
    }


}
