package testPackage;
import java.util.ArrayList;

//Интерфейс для ожерелья
public interface ForNecklace {
    int caratCounting();
    void sortingStone();
    void outPut();
    ArrayList<ForStone> searchStone(int beginning, int end);
}
