package com.company;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class NecklaceChange implements ForNecklace {
    private Scanner in;
    private ArrayList<Stone> stones;

    public NecklaceChange() {
        this.in = new Scanner(System.in);
        this.stones = new ArrayList();
    }

    public void createNecklace(int amountStones) throws IOException {
        String fileName = "file.txt";
        String[] content = ((String)Files.lines(Paths.get(fileName)).reduce("", String::concat)).split(" ");

        this.stones.clear();
        System.out.println("Доступные камни: ");
        int count = 1;

        for(int i = 0; i < content.length - 2; i += 3) {
            System.out.println(count + ". " + content[i] + "  карат: " + content[i + 1] + " тип: " + content[i + 2]);
            ++count;
        }

        System.out.println("Выберите камни для ожерелья: ");
        int[] numberStone = new int[amountStones];

        int i;
        for(i = 0; i < amountStones; ++i) {
            numberStone[i] = this.in.nextInt() - 1;
        }

        count = 0;

        for(i = 0; i < amountStones; ++i) {
            this.stones.add(new Stone(content[numberStone[count] * 3], Integer.parseInt(content[numberStone[count] * 3 + 1]), content[numberStone[count] * 3 + 2]));
            ++count;
        }

        Iterator var9 = this.stones.iterator();

        while(var9.hasNext()) {
            Stone item = (Stone)var9.next();
            System.out.println(item);
        }

    }

    public ArrayList<Stone> getStone() {
        return this.stones;
    }

    public void outPut() {
        System.out.println(this.stones.toString());
    }

    public int caratCounting() {
        int carat = this.stones.stream().mapToInt((v) -> {
            return v.getCarat();
        }).sum();
        return carat;
    }

    public void sortingStone() {
        this.stones.sort((o1, o2) -> {
            return o1.getCarat() - o2.getCarat();
        });
    }

    public ArrayList<ForStone> searchStone(int beginning, int end) {
        ArrayList<ForStone> stonesToSearch = new ArrayList();

        for(int i = 0; i < this.stones.size(); ++i) {
            if (((Stone)this.stones.get(i)).getClarity() >= beginning && ((Stone)this.stones.get(i)).getClarity() <= end) {
                stonesToSearch.add(this.stones.get(i));
            }
        }

        return stonesToSearch;
    }
}

