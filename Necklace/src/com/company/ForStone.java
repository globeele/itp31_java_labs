package com.company;

public interface ForStone {
    String toString();

    double getClarity();

    int getCarat();

    double getAmount();

    String getName();

    String getType();
}

