package com.company;


public class Stone implements ForStone {
    private String name;
    private int carat;
    private String type;
    private double clarity;
    private double amount;

    Stone(String name, int carat, String type) {
        this.name = name;
        this.carat = carat;
        this.type = type;
    }

    public Stone(String s, int parseInt) {
    }

    public String getName() {
        return this.name;
    }

    public int getCarat() {
        return this.carat;
    }

    public double getClarity(){return  this.carat * 1.5;}

    public String getType() {
        return this.type;
    }

    public double getAmount(){return  this.carat * 5;}

    public String toString() {
        return this.type.equals("s") ? "Драгоценные{name='" + this.name + '\'' + ", carat=" + this.carat + '\'' +
                ", type='" + this.type + '\'' +  ", clarity=" + this.getClarity() + '\''  + ", amount=" + this.getAmount() + '\''  +'}' :
                "Полудрагоценные{name='" + this.name + '\'' + ", carat=" + this.carat + '\'' + "type='" + this.type + '\'' +
                        ", clarity=" + this.getClarity() + '\''  +  ", amount=" + this.getAmount() + '\''  +'}';
    }
}

