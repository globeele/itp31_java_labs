package com.company;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public Main() {
    }

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        List<ForNecklace> forNecklaces = new ArrayList();
        Necklace necklace = new Necklace();
        NecklaceChange necklaceChange = new NecklaceChange();
        forNecklaces.add(necklace);
        forNecklaces.add(necklaceChange);

        while(true) {
            int index = Menu();
            switch(index) {
                case 1:
                    System.out.println("Введите количество камней в ожерелье №1:");
                    int enter = in.nextInt();
                    necklace.createNecklace(enter);
                    necklace.outPut();
                    System.out.println("Введите количество камней в ожерелье №2:");
                    enter = in.nextInt();
                    necklaceChange.createNecklace(enter);
                    necklaceChange.outPut();
                    break;
                case 2:
                    System.out.println("Карат в ожерелье №1: " + necklace.caratCounting());
                    System.out.println("Карат в ожерелье №2: " + necklaceChange.caratCounting());
                    break;
                case 3:
                    forNecklaces.forEach(ForNecklace::sortingStone);
                    forNecklaces.forEach(ForNecklace::outPut);
                    break;
                case 4:
                    System.out.println("Введите диапозон прозрачности:");
                    ArrayList<ForStone> stonesFromnecklace = necklace.searchStone(in.nextInt(), in.nextInt());
                    outPutDiap(stonesFromnecklace);
                    System.out.println("Введите диапозон прозрачности:");
                    ArrayList<ForStone> stonesFromnecklaceChange = necklaceChange.searchStone(in.nextInt(), in.nextInt());
                    outPutDiap(stonesFromnecklaceChange);
                    break;
                case 5:
                    return;
            }
        }
    }

    static void outPutDiap(ArrayList<ForStone> stones) {
        if (stones.size() == 0) {
            System.out.println("В данном диапазоне нет камней!!!");
        }
        System.out.println(stones);

//        for(int i = 0; i < stones.size(); ++i) {
//            System.out.println(((ForStone)stones.get(i)).getName());
//            System.out.println(((ForStone)stones.get(i)).getCarat());
//            System.out.println(((ForStone)stones.get(i)).getType());
//            System.
//        }

    }

    static int Menu() throws IOException {
        Scanner in = new Scanner(System.in);
        System.out.println("1. Создать ожерелье");
        System.out.println("2. Посчитать ценность в каратах");
        System.out.println("3. Сортировка камней");
        System.out.println("4. Найти камни в диапозоне");
        System.out.println("5. Выход");
        System.out.println("Введите номер меню");
        int index = in.nextInt();
        return index;
    }
}
