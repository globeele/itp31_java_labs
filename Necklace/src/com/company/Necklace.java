package com.company;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Scanner;

public class Necklace implements ForNecklace {
    private Scanner in;
    private ArrayList<ForStone> stones;

    public Necklace() {
        this.in = new Scanner(System.in);
        this.stones = new ArrayList();
    }

    public void createNecklace(int amountStones) throws IOException {
        String fileName = "file.txt";
        String[] content = ((String)Files.lines(Paths.get(fileName)).reduce("", String::concat)).split(" ");
        String type = content[2];
        this.stones.clear();
        System.out.println("Доступные камни: ");
        int count = 1;

        for(int i = 0; i < content.length - 2; i += 3) {
            System.out.println(count + ". " + content[i] + "  карат: " + content[i + 1] + " тип: " + content[i + 2]);
            ++count;
        }

        System.out.println("Выберите камни для ожерелья: ");
        int[] numberStone = new int[amountStones];

        int i;
        for(i = 0; i < amountStones; ++i) {
            numberStone[i] = this.in.nextInt() - 1;
        }

        count = 0;

        for(i = 0; i < amountStones; ++i) {
            if (type.equals("s")) {
                this.stones.add(new Stone(content[numberStone[count] * 3],
                        Integer.parseInt(content[numberStone[count] * 3 + 1]),
                        content[numberStone[count] * 3 + 2]));
            } else {
                if ((type.equals("ps")))
                {
                    this.stones.add(new PStone(content[numberStone[count] * 3],
                            Integer.parseInt(content[numberStone[count] * 3 + 1]),
                            content[numberStone[count] * 3 + 2]));
                }
            }


            ++count;
        }

        Iterator var11 = this.stones.iterator();

        while(var11.hasNext()) {
            ForStone item = (ForStone)var11.next();
            System.out.println(item);
        }

    }

    public ArrayList<ForStone> getStones() {
        return this.stones;
    }

    public void outPut() {
        System.out.println(this.stones);
    }

    public int caratCounting() {
        int carat = this.stones.stream().mapToInt(v -> v.getCarat()).sum();
        return carat;
    }

    public void sortingStone() {
        Collections.sort(this.stones, new Comparator<ForStone>() {
            public int compare(ForStone o1, ForStone o2) {
                if (o1.getAmount() > o2.getAmount()) return 1;
                else if (o1.getAmount() < o2.getAmount()) return -1;
                else return 0;
            }
        });
    }

    public ArrayList<ForStone> searchStone(int beginning, int end) {
        ArrayList<ForStone> stonesToSearch = new ArrayList();

        for(int i = 0; i < this.stones.size(); ++i) {
            if ((this.stones.get(i)).getClarity() >= beginning && (this.stones.get(i)).getClarity() <= end) {
                stonesToSearch.add(this.stones.get(i));
            }
        }

        return stonesToSearch;
    }
}

