package com.company;

public class PStone implements ForStone {
    private String name;
    private int carat;
    private String type;
    private double clarity;
    private double amount;

    PStone(String name, int carat, String type) {
        this.name = name;
        this.carat = carat;
        this.type = type;
    }

    public PStone(String s, int parseInt) {
    }

    public double getClarity(){return  this.carat * 1.5;}
    public String getName() {
        return this.name;
    }
    public double getAmount(){return  this.carat * 2.5;}
    public int getCarat() {
        return this.carat;
    }

    public String getType() {
        return this.type;
    }

    public String toString() {
        return this.type.equals("ps") ? "Полудрагоценные{name='" + this.name + '\'' + ", carat=" + this.carat + '\'' +
                ", type='" + this.type + '\'' +  ", clarity=" + this.getClarity() + '\''  + ", amount=" + this.getAmount() + '\''  +'}' :
                "Драгоценные{name='" + this.name + '\'' + ", carat=" + this.carat + '\'' + "type='" + this.type + '\'' +
                        ", clarity=" + this.getClarity() + '\''  + ", amount=" + this.getAmount() + '\''  +'}';
    }
}
